#!/usr/bin/env bash

clang-format -i --style=file $(git ls-files | grep -E -- '\.(c|h)$')
