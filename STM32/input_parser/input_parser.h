#pragma once

/**
 * @brief Iterator state.
 */
typedef enum {
	IPRS_CONTINUE, ///< Continue to supply input
	IPRS_MATCH,    ///< Found match
	IPRS_MISS,     ///< None of strings match
} iprs_result;

/**
 * @brief String container. Strings must be NULL terminated.
 */
typedef struct {
	struct {
		unsigned char pos;	///< position in string
		unsigned char miss : 1; ///< string miss
	} flags;
	char *string; ///< string
} iprs_string;

/**
 * @brief Internal state of parser. Should not be read or changed outside of library functions.
 */
typedef struct {
	iprs_string  *strings;	 ///< list of strings
	unsigned char count;	 ///< list of strings count
	unsigned char match;	 ///< match index
	char	      reset_chr; ///< reset character
	struct {
		unsigned char use_reset_chr : 1; ///< use reset character
		unsigned char match	    : 1; ///< match found
		unsigned char miss	    : 1; ///< none of strings match
		unsigned char stop_on_match : 1; ///< stop when match is found
		unsigned char autoreset	    : 1; ///< autoreset
	} flags;
} iprs_state;

/**
 * @brief Initialize state.
 *
 * @param[out] state State of the parser.
 * @param[in] strings List of strings.
 * @param[in] strings_count Size of \p strings array.
 * @param[in] reset Reset to zero position when break character is read. Supply -1 to disable reset char.
 * @param[in] stop_on_match Stop when match found. Otherwise return \p IPRS_MISS on extra input or reset on \p reset
 * @param[in] autoreset Reset state instead of throwing \p IPRS_MISS.
 */
void iprs_init_state(iprs_state *state, iprs_string *strings, unsigned char strings_count, char reset,
		     int stop_on_match, int autoreset);

/**
 * @brief Reset state.
 * @param[in,out] state State of the parser.
 */
void iprs_reset(iprs_state *state);

/**
 * @brief Match input.
 *
 * @param[in,out] state State of the parser.
 * @param[in] buffer Input buffer.
 * @param[in] size Input buffer size.
 * @returt Match status.
 */
iprs_result iprs_process(iprs_state *state, char *buffer, unsigned char size);

/**
 * @brief Get state status.
 *
 * @param[in] state State of the parser.
 * @return Match status.
 */
iprs_result iprs_status(iprs_state *state);

/**
 * @brief Get match.
 *
 * @param[in] state State of the parser.
 * @return Match string index. If \p iprs_status is not \p IPRS_MATCH, output of this command is undefined.
 */
unsigned char iprs_match(iprs_state *state);
