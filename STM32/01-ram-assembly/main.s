	.syntax unified

	.section .data
gpioc_base:
	.word 0x40011000
gpioc_crh:
	.word 0x04
gpioc_odr:
	.word 0x0c
gpio_set:
	.word 0x44144444
rcc_apb2enr:
	.word 0x18
rcc_base:
	.word 0x40021000
rcc_set:
	.word (1<<4)
led_on:
	.word 0
led_off:
	.word (1<<13)
sleep:
	.word 0x80000

	.section .text

	.global Default_Handler
	.func Default_Handler
	.thumb_func
Default_Handler:
Infinite_Loop:
	b Infinite_Loop

	.endfunc

	@@ this directive is needed for
	@@ linker to generate address of Reset_Handler
	@@ in thumb mode
	.global Reset_Handler
	.func Reset_Handler
	.thumb_func
Reset_Handler:
	ldr ip, =rcc_base
	ldr r1, [ip]
	ldr ip, =rcc_apb2enr
	ldr r2, [ip]
	ldr ip, =rcc_set
	ldr r3, [ip]
	add r0, r1, r2
	str r3, [r0]
	ldr ip, =gpioc_base
	ldr r1, [ip]
	ldr ip, =gpioc_crh
	ldr r2, [ip]
	add r0, r1, r2
	ldr ip, =gpio_set
	ldr r3, [ip]
	str r3, [r0]
	ldr ip, =gpioc_odr
	ldr r2, [ip]
	add r0, r1, r2
	ldr ip, =led_off
	ldr r1, [ip]
	ldr ip, =led_on
	ldr r2, [ip]

Setup:
loop:
	str r2, [r0]
	ldr ip, =sleep
	ldr r3, [ip]
0:
	cmp r3, #0
	sub r3, r3, #1
	bne 0b
	nop
	str r1, [r0]
	ldr ip, =sleep
	ldr r3, [ip]
0:
	cmp r3, #0
	sub r3, r3, #1
	bne 0b
	nop
	b loop
	nop

	.endfunc

	.end
