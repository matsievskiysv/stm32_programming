#include <adc.h>
#include <dma.h>
#include <gpio.h>
#include <nvic.h>
#include <rcc.h>
#include <stdint.h>
#include <systick.h>
#include <util.h>

#ifndef LED_PORT
#define LED_PORT GPIO_C
#endif // LED_PORT

#ifndef LED_PIN
#define LED_PIN 13
#endif // LED_PIN

void calibrate_temp_sensor(void);

struct job_list_t {
	unsigned short int count;
	unsigned short int current;
	void (*callback)(void);
};

volatile uint16_t	   temp_raw[2] = {0};
volatile int32_t	   temp;
volatile struct job_list_t job_list[] = {
    {.count = 60, .current = 0, .callback = calibrate_temp_sensor},
    {.count = 10, .current = 0, .callback = adc_temp_start},
    {0},
};

void
systick_handler(void)
{
	for (unsigned int i = 0; job_list[i].callback != NULL; i++) {
		if (job_list[i].current == 0) {
			job_list[i].callback();
			job_list[i].current = job_list[i].count;
		} else {
			job_list[i].current--;
		}
	}
}

void
dma1_channel1_irqhandler(void)
{
	dma_interrupt_t mask = {
	    .bits = {
		.global = true, .half_transfer_complete = true, .transfer_complete = true, .transfer_error = true}};
	dma_int_clear(DMA1, DMA_CH_ADC1, mask);
	gpio_toggle_pin(LED_PORT, LED_PIN);
	temp = adc_temp_convert(temp_raw[0], temp_raw[1]);
}

void
calibrate_temp_sensor(void)
{
	adc_calibrate(ADC1);
}

int __attribute__((noreturn)) main(void)
{
	// enable clocks
	rcc_start_internal_clock();
	rcc_clock_t clock = {.apb2 = {.bits = {.iopcen = true, .adc1en = true}}, .ahb = {.bits = {.dma1en = true}}};
	rcc_clock_enable(clock);
	// configure LED
	gpio_configure_output(LED_PORT, LED_PIN, GPIO_OUT_PUSH, GPIO_SPEED_LOW);
	gpio_lock(LED_PORT, BIT(LED_PIN));
	gpio_write_pin(LED_PORT, LED_PIN, true);

	// configure systick (1sec at 1MHz)
	systick_configure(8000000, true);

	// configure DMA
	adc_t *adc1 = GET_REG(ADC1);
	dma_config(DMA1, DMA_CH_ADC1, temp_raw, (void *) &adc1->dr.val, 2, false, DMA_PRIORITY_MEDIUM, DMA_SIZE_16BIT,
		   DMA_SIZE_16BIT, true, false, true, false, false, false, true);
	dma_enable(DMA1, DMA_CH_ADC1);
	enable_interrupt(DMA1_CH1_INT);

	// configure temp sensor
	rcc_adc_prescaler(RCC_ADCPRE_8);
	adc_temp_config(false, true);

	// start clock
	systick_start();

	LOOP();
}
