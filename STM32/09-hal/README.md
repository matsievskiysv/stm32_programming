# Hardware abstraction layer (HAL)

In our previous example `main` function got so big and unreadable we had to split it into functions for each peripheral. It made code more readable but it still had major problems:
* for the most part code is unreadable without datasheet;
* program has to be completely rewritten for other controllers (maybe even of the same family);
* there's no code reuse.

The usual way to deal with these problems is to introduce Hardware abstraction layer (HAL). The idea is to put hardware specific actions into functions that provide relatively high level interfaces to microcontroller's internal functions and peripherals. Then, swapping microcontroller to another would only require changing HAL implementation without modifying program logic. Additionally, HAL functions usually have descriptive function names like `timer_start` which improve code readability. HAL libraries are compiled as statically linked libraries. During program linking only used functions will get into final binary, so there's no downside in using them.

STM32 controllers have vendor provided HAL libraries but we will write our own. Note that this HAL serves educational purposes and should not be used in real applications.

## Standard library

So far we didn't use C standard library functions. It would soon get tedious re-implementing and debugging standard library functions and definitions ourselves, so we will add [newlib](https://sourceware.org/newlib/) to our project. Newlib is a popular implementation of standard library functions designed specifically for embedded applications.

Firstly, we have to compile library for our specific architecture. Note, that STM32 chips do not work with functions compiled in arm mode and require thumb mode functions. Therefore, some precompiled binaries (e.g. [debian packages](https://packages.debian.org/sid/libnewlib-arm-none-eabi)) may not raise compiler warnings but will put microcontroller in HardFault. We will use the same compiler toolchain and compilation options as for our program to build newlib.

## Standard library data types

One of the reasons to pull Newlib into our project was the desire to use standard C types. We will use
* `size_t` -- used to hold register addresses;
* `bool` -- `true` or `false` values;
* `stdint.h` -- used for explicit data sizes.

## Shorthand macros

Frequently HALs implement shorthand macros for read-modify-write operations. We will define some macros to make code clearer.

We always want our peripheral registers to be declared as `vilatile`. So we add special macro for peripheral registers
```c
#define REG_T volatile size_t *const
```
We usually express register addresses as `base` and `offset` pairs. So, we declare macro for retrieving absolute register address
```c
#define GET_REG(BASE, OFFSET) ((size_t *) ((BASE) + (OFFSET)))
```
Other bit macros are self explanatory
```c
#define BIT(X) (1 << X)
#define REG_VAL(REG) (*REG)
#define REG_WRITE(REG, VAL) *REG = (VAL)
#define REG_SET(REG, MASK) *REG |= (MASK)
#define REG_CLEAR(REG, MASK) *REG &= ~(MASK)
#define REG_TOGGLE(REG, MASK) *REG ^= (MASK)
```
These make our code more attractive. For example, this snippet hides almost all boilerplate code.
```c
REG_T data = GET_REG(bank, GPIOx_ODR);
bool  val  = !!(REG_VAL(data) & BIT(pin));
REG_T reg  = GET_REG(bank, val ? GPIOx_BRR : GPIOx_BSSR);
REG_WRITE(reg, BIT(pin));
```

## HAL structure

It's important for HAL to be easily extendable for use with other chip models. It is achieved by using C preprocessor `ifdef` directives. We have the following directory structure of our HAL
```
hal
├── include
├── link
├── stm32f103h
├── stm32f103m
└── stm32f10x
```
`include` folder define headers which define function interfaces and macros. Each header in this folder loads chip specific macros using construct similar to this
```c
#if defined(stm32f101l)
#error Not implemented
#elif defined(stm32f101m)
#error Not implemented
#elif defined(stm32f101h)
#error Not implemented
#elif defined(stm32f102l)
#error Not implemented
#elif defined(set32f102m)
#error Not implemented
#elif defined(stm32f103l)
#error Not implemented
#elif defined(stm32f103m)
#include "../stm32f103m/gpio.h"
#elif defined(set32f103h)
#include "../stm32f103h/gpio.h"
#elif defined(stm32f105)
#error Not implemented
#elif defined(stm32f107)
#error Not implemented
#else
#error Board not selected
#endif
```
It selects appropriate header from chip family folders based on definition. Notice placeholders `#error Not implemented` which will stop compilation if unsupported board was selected. The rest of the file are common declarations (documentation lines are omitted)
```c
#define GPIO_INPUT  0b00##UL
#define GPIO_OUTPUT 0b01##UL
#define GPIO_ALT    0b10##UL

#define GPIO_PUSHPULL 0b00##UL
#define GPIO_OPEN     0b01##UL
#define GPIO_ANALOG   0b10##UL
#define GPIO_FLOAT    0b11##UL

#define GPIO_SPEED_LOW GPIOx_SPEED_2MHZ
#define GPIO_SPEED_MED GPIOx_SPEED_10MHZ
#define GPIO_SPEED_MAX GPIOx_SPEED_50MHZ

void gpio_configure(size_t bank, size_t pin, size_t direction, size_t mode, size_t speed);
void gpio_lock(size_t bank, size_t pin_mask);
size_t gpio_read_bank(size_t bank);
bool gpio_read_pin(size_t bank, size_t pin);
void gpio_write_pin(size_t bank, size_t pin_mask, bool value);
void gpio_toggle_pin(size_t bank, size_t pin);
```
These macros and functions have descriptive names and do not require datasheet to understand them. For example, the following snippet does not require comments to understand it.
```c
rcc_start_clock(RCC_APB2ENR, 4);
gpio_configure(LED_PORT, LED_PIN, GPIO_OPEN, GPIO_PUSHPULL, GPIO_SPEED_LOW);
gpio_lock(LED_PORT, BIT(LED_PIN));
gpio_write_pin(LED_PORT, BIT(LED_PIN), true);
```

## Preprocessor `undef`

In our particular case all the registers are common for `stm32f10x` families and described in single datasheet. All the chips in these families implement a subset of functions described in this datasheet. Thus it makes sense to define common headers for `stm32f10x` and then just turn off things that are not implemented. We are doing this by using `undef` preprocessor directive.
For example, `stm32f10x` defines the following timers
```c
#define TIM1  0x4001##2C00##UL
#define TIM2  0x4000##0000##UL
#define TIM3  0x4000##0400##UL
#define TIM4  0x4000##0800##UL
#define TIM5  0x4000##0C00##UL
#define TIM6  0x4000##1000##UL
#define TIM7  0x4000##1400##UL
#define TIM8  0x4001##3400##UL
#define TIM9  0x4001##4C00##UL
#define TIM10 0x4001##5000##UL
#define TIM11 0x4001##5400##UL
#define TIM12 0x4000##1800##UL
#define TIM13 0x4000##1C00##UL
#define TIM14 0x4000##2000##UL
```
But `stm32f103m` has only a small portion of those timers. So we include `stm32f10x` header with timer definitions and remove unimplemented timers.
```c
#include <../stm32f10x/timer.h>

#undef TIM5
#undef TIM6
#undef TIM7
#undef TIM8
#undef TIM9
#undef TIM10
#undef TIM11
#undef TIM12
#undef TIM13
#undef TIM14
```
So, when programmer will try to use unimplemented timer in his code, compiler will throw error.

## Notes about linker script

### Linker script variables

All our boards could use the same linker script but have different RAM and Flash memory sizes. To avoid copy-pasting linker scripts for each chip family we pass these values as parameters `__flashsize` and `__ramsize`.
```
MEMORY {
	FLASH	(rx)	: ORIGIN = 0x08000000,	LENGTH = __flashsize
	RAM	(rwx)	: ORIGIN = 0x20000000,	LENGTH = __ramsize
}
```
During the compilation we assign them using command line options
```bash
-Wl,--defsym,__ramsize=$(RAMSIZE) -Wl,--defsym,__flashsize=$(FLASHSIZE)
```

### Force load symbols

Chip reset vector will be compiled into HAL static library. Because code functions do not explicitly reset vector symbols, they will not be pulled into final binary, which is not what we want. In order to instruct compiler to add reset vector into the final binary we add compiler option `-u__isr_vector`.

## `Makefile` and linker script arguments

Now we have many different boards supported (two). It will get tedious to type chip family and memory sizes for each `make` command we use. So we put these variables into `config.mk` file and include it into our main `Makefile`. Note that because of `?=` operator these variables may be overwritten form the command line.
```
CHIP?=stm32f103m
RAMSIZE?=20k
FLASHSIZE?=64k
DEFS_EXTRA?=
```

## Maintaining libraries

When writing HAL you should follow library writer best practices (I do not claim to know all of them):

1. Do not break library interface.
   1. If you have the same functionality implemented in different way, use different function name. Function names like `write_gpio` and `write_gpio2` may indicate different implementations.
   1. Use `deprecated`, `warning` and `error` function attributes to mark functions.
1. Deprecate functions gradually. Leave deprecated function for some time with `deprecated` attribute to make users time to switch to newer implementation.
1. If you rely on specific compiler features, write down the compiler name and version in HAL documentation. It may be better detect compiler at compilation time and include optimized code regions conditionally.
1. Explicitly set C standard version and write down code formatting rules ([code beautifiers](https://clang.llvm.org/docs/ClangFormat.html) may be of help) to enforce code uniformity.
