# UART

```mermaid
stateDiagram-v2
  prompt : Prompt
  listen : Listen
  match : Match
  action : Action
  help : Miss handle
  [*] --> prompt
  prompt --> listen
  listen --> match
  match --> action : Match
  match --> help : Miss
  match --> listen : Continue
  listen --> help : Overflow
  help --> prompt
  action --> prompt
```
