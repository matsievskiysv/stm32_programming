        .syntax unified

	.section .data
gpioc_base:
        .long 0x40011000
gpioc_crh:
        .long 0x04
gpioc_odr:
        .long 0x0c
gpio_set:
        .long 0x44144444
rcc_apb2enr:
        .long 0x18
rcc_base:
        .long 0x40021000
rcc_set:
        .long (1<<4)
led_on:
        .long 0
led_off:
        .long (1<<13)
sleep:
        .long 0x100000


	.section .boot, "xa"

	.func Default_Handler
	.global Default_Handler
	.thumb_func
Default_Handler:
Infinite_Loop:
        b Infinite_Loop

	.endfunc

	.func copy_loop
	.global copy_loop
	.thumb_func
copy_loop:
	ldr ip, [r2]
	str ip, [r0]
	add r0, r0, #4
	add r2, r2, #4
	cmp r1, r0
	bgt copy_loop
	bx lr

	.endfunc

	.func Reset_Handler
	.global Reset_Handler
	.thumb_func  /*this directive is needed for
	linker to generate address of Reset_Handler
	in thumb mode*/
Reset_Handler:
	ldr r0, = _text_vma
	ldr r1, = _text_evma
	ldr r2, = _text_lma

	bl copy_loop

	ldr r0, = _data_vma
	ldr r1, = _data_evma
	ldr r2, = _data_lma

	bl copy_loop

	ldr lr, =Blink
	bx lr

	.endfunc


	.section .text

	.func Blink
	.global Blink
	.thumb_func
Blink:
	ldr ip, =rcc_base
	ldr r1, [ip]
	ldr ip, =rcc_apb2enr
	ldr r2, [ip]
	ldr ip, =rcc_set
	ldr r3, [ip]
	add r0, r1, r2
	str r3, [r0]
	ldr ip, =gpioc_base
	ldr r1, [ip]
	ldr ip, =gpioc_crh
	ldr r2, [ip]
	add r0, r1, r2
	ldr ip, =gpio_set
	ldr r3, [ip]
	str r3, [r0]
	ldr ip, =gpioc_odr
	ldr r2, [ip]
	add r0, r1, r2
	ldr ip, =led_off
	ldr r1, [ip]
	ldr ip, =led_on
	ldr r2, [ip]

Setup:
loop:
	str r2, [r0]
	ldr ip, =sleep
	ldr r3, [ip]
0:
	cmp r3, #0
	sub r3, r3, #1
	bne 0b
	nop
	str r1, [r0]
	ldr ip, =sleep
	ldr r3, [ip]
0:
	cmp r3, #0
	sub r3, r3, #1
	bne 0b
	nop
	b loop
	nop

	.endfunc

	.end
