# `.text` section relocation

With clock speed above 100MHz fetching instructions from Flash becomes a performance bottleneck, so prior to the execution program instructions may be relocated to RAM. Relocation of `.text` section requires introduction of additional executable section residing in Flash. Let's call this section `.boot`.
As opposed to `.text`, `.data` etc. conventional sections `.boot` section name is not conventional. To mark this section as executable section we use assembly `section` directive with `x` flag. Note, that section also must have `a` flag set so that linker would allocate memory for this section.
```assembly
  .section .boot, "xa"
```
This section will contain all the relocation instructions.

> STM32F103 has limited RAM and CPU speed which makes `.text` section relocation undesirable. Therefore, `.text` section will not be relocated in other exercises.
{.is-info}

### Exercises

1. Make AAPCS complaint function for memory region relocation.
