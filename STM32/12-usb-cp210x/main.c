#include "../input_parser/input_parser.h"

#include <flash.h>
#include <gpio.h>
#include <nvic.h>
#include <rcc.h>
#include <string.h>
#include <systick.h>
#include <usb_cp210x.h>
#include <util.h>

#define USB_GPIO_PORT GPIO_A
#define USB_PIN_DP    12
#define USB_PIN_DM    11

extern usb_btable_entry_t SHARED_SRAM_REGION usb_btable[3];
extern size_t SHARED_SRAM_REGION
    usb_buffer_cp210x_data_rx[SHARED_SRAM_ARRAY_SIZE(USB_CP210X_DATA_BUFFER_SIZE_64, size_t)];
extern size_t SHARED_SRAM_REGION
    usb_buffer_cp210x_data_tx[SHARED_SRAM_ARRAY_SIZE(USB_CP210X_DATA_BUFFER_SIZE_64, size_t)];
extern size_t SHARED_SRAM_REGION
    usb_buffer_cp210x_control_rx[SHARED_SRAM_ARRAY_SIZE(USB_CP210X_DATA_BUFFER_SIZE_64, size_t)];
extern size_t SHARED_SRAM_REGION
    usb_buffer_cp210x_control_tx[SHARED_SRAM_ARRAY_SIZE(USB_CP210X_DATA_BUFFER_SIZE_64, size_t)];

iprs_state parser_state = {0};

typedef enum {
	IDX_HELP,
	IDX_NONE,
	IDX_UPPER,
	IDX_LOWER,
} iprs_string_command_idx_t;

iprs_string commands[] = {
    [IDX_HELP]	= {.string = "help\r"},
    [IDX_NONE]	= {.string = "none\r"},
    [IDX_UPPER] = {.string = "upper\r"},
    [IDX_LOWER] = {.string = "lower\r"},
};

typedef enum {
	INPUT_TRANSFORM_NONE,
	INPUT_TRANSFORM_UPPER,
	INPUT_TRANSFORM_LOWER,
} input_transform_t;

static volatile input_transform_t transform = INPUT_TRANSFORM_NONE;

static char help_string[] = "\r\nCommands:\r\n"
			    "help: this message\r\n"
			    "none: no transformation\r\n"
			    "upper: upcase input\r\n"
			    "lower: lowercase input\r\n";

void
usb_cp210x_data_rx_handler(uint8_t ep)
{
	size_t	count = usb_btable[ep].count_rx.bits.countn_rx;
	uint8_t line[count];
	memcpy_shared2ram(line, usb_buffer_cp210x_data_rx, count);
	switch (transform) {
	case INPUT_TRANSFORM_UPPER: {
		for (size_t idx = 0; idx < count; idx++) {
			if (line[idx] >= 'a' && line[idx] <= 'z')
				line[idx] = line[idx] - 'a' + 'A';
		}
		break;
	}
	case INPUT_TRANSFORM_LOWER: {
		for (size_t idx = 0; idx < count; idx++) {
			if (line[idx] >= 'A' && line[idx] <= 'Z')
				line[idx] = line[idx] - 'A' + 'a';
		}
		break;
	}
	default:
		break;
	}
	usb_endpoint_transfer_single(ep, line, count);
	usb_endpoint_start_tx(ep);
}

void
usb_cp210x_control_rx_handler(uint8_t ep)
{
	uint8_t count = usb_btable[ep].count_rx.bits.countn_rx;
	uint8_t line[count];
	memcpy_shared2ram(line, usb_buffer_cp210x_control_rx, count);
	iprs_process(&parser_state, (char *) line, count);
	switch (iprs_status(&parser_state)) {
	case IPRS_CONTINUE:
		usb_endpoint_transfer_single(ep, line, count);
		usb_endpoint_start_tx(ep);
		break;
	case IPRS_MISS: {
		char message[] = "unknown command\r\n";
		usb_endpoint_transfer_single(ep, message, sizeof(message));
		iprs_reset(&parser_state);
		usb_endpoint_start_tx(ep);
		break;
	}
	case IPRS_MATCH:
		switch ((iprs_string_command_idx_t) iprs_match(&parser_state)) {
		case IDX_NONE: {
			transform      = INPUT_TRANSFORM_NONE;
			char message[] = "\r\n";
			usb_endpoint_transfer_single(ep, message, sizeof(message));
			break;
		}
		case IDX_UPPER: {
			transform      = INPUT_TRANSFORM_UPPER;
			char message[] = "\r\n";
			usb_endpoint_transfer_single(ep, message, sizeof(message));
			break;
		}
		case IDX_LOWER: {
			transform      = INPUT_TRANSFORM_LOWER;
			char message[] = "\r\n";
			usb_endpoint_transfer_single(ep, message, sizeof(message));
			break;
		}
		default: {
			usb_endpoint_transfer_string_begin(ep, help_string);
			break;
		}
		}
		usb_endpoint_start_tx(ep);
		iprs_reset(&parser_state);
		break;
	}
}

usb_global_state_t usb_global_state = {
    .endpoint_count = ARRAY_SIZE(usb_btable),
    .endpoints =
	{
	    {
		.endpoint_type	= USB_ENDPOINT_TYPE_CONTROL,
		.address	= 0,
		.rx_buffer_size = USB_EP0_SIZE,
		.rx_buffer	= usb_buffer_ep0_rx,
		.tx_buffer_size = USB_EP0_SIZE,
		.tx_buffer	= usb_buffer_ep0_tx,
		.rx_callback	= usb_handle_cp210x_setup,
	    },
	    {
		.endpoint_type	= USB_ENDPOINT_TYPE_BULK,
		.address	= 1,
		.rx_buffer_size = 64,
		.rx_buffer	= usb_buffer_cp210x_data_rx,
		.tx_buffer_size = 64,
		.tx_buffer	= usb_buffer_cp210x_data_tx,
		.rx_callback	= usb_cp210x_data_rx_handler,
		.tx_callback	= usb_handle_transfer,
	    },
	    {
		.endpoint_type	= USB_ENDPOINT_TYPE_BULK,
		.address	= 2,
		.rx_buffer_size = 64,
		.rx_buffer	= usb_buffer_cp210x_control_rx,
		.tx_buffer_size = 64,
		.tx_buffer	= usb_buffer_cp210x_control_tx,
		.rx_callback	= usb_cp210x_control_rx_handler,
		.tx_callback	= usb_handle_transfer,
	    },
	},
};

usb_cp210x_global_state_t usb_cp210x_global_state = {0};

void
usb_lp_can1_rx0_irqhandler(void)
{
	usb_irqhandler();
}

int __attribute__((noreturn)) main(void)
{
	// enable clocks
	{
		rcc_start_pll_clock(true, false, RCC_PLLMUL_9X, RCC_PPRE1_2, RCC_PPRE2_1, RCC_HPRE_2, RCC_ADCPRE_8,
				    true, FLASH_LATENCY_2);
		rcc_clock_t clock = {.apb1 = {.bits = {.usben = true}}, .apb2 = {.bits = {.iopaen = false}}};
		rcc_clock_enable(clock);
	}

	iprs_init_state(&parser_state, commands, ARRAY_SIZE(commands), '\n', false, false);
	enable_interrupt(USB_LP_INT);

	usb_t *usb = GET_REG(USB);
	{
		usb_daddr_t addr = {.bits = {.ef = false, .add = 0}};
		REG_WRITE(&usb->daddr.val, addr.val);
	}
	{
		usb_cntr_t cntr = {.bits = {.fsusp = true, .lp_mode = true, .pdwn = true}};
		REG_CLEAR(&usb->cntr.val, cntr.val);
	}
	systick_wait(100);	      // wait for at least 1µs
	REG_WRITE(&usb->istr.val, 0); // clear spurious interrupts
	{
		usb_cntr_t cntr = {.bits = {.resetm = true, .ctrm = true, .errm = false}};
		REG_SET(&usb->cntr.val, cntr.val);
	}
	{
		usb_cntr_t cntr = {.bits = {.fres = true}};
		REG_CLEAR(&usb->cntr.val, cntr.val);
	}

	LOOP();
}
