#include <usb_cp210x.h>
#include <util.h>

usb_btable_entry_t SHARED_SRAM_REGION usb_btable[3];
size_t SHARED_SRAM_REGION	      usb_buffer_ep0_rx[SHARED_SRAM_ARRAY_SIZE(USB_EP0_SIZE, size_t)];
size_t SHARED_SRAM_REGION	      usb_buffer_ep0_tx[SHARED_SRAM_ARRAY_SIZE(USB_EP0_SIZE, size_t)];
size_t SHARED_SRAM_REGION usb_buffer_cp210x_data_rx[SHARED_SRAM_ARRAY_SIZE(USB_CP210X_DATA_BUFFER_SIZE_64, size_t)];
size_t SHARED_SRAM_REGION usb_buffer_cp210x_data_tx[SHARED_SRAM_ARRAY_SIZE(USB_CP210X_DATA_BUFFER_SIZE_64, size_t)];
size_t SHARED_SRAM_REGION usb_buffer_cp210x_control_rx[SHARED_SRAM_ARRAY_SIZE(USB_CP210X_DATA_BUFFER_SIZE_64, size_t)];
size_t SHARED_SRAM_REGION usb_buffer_cp210x_control_tx[SHARED_SRAM_ARRAY_SIZE(USB_CP210X_DATA_BUFFER_SIZE_64, size_t)];

typedef enum {
	USB_DESCRIPTOR_STRING_IDX_MFR,
	USB_DESCRIPTOR_STRING_IDX_PRODUCT,
	USB_DESCRIPTOR_STRING_IDX_SERIALNUMBER,
	USB_DESCRIPTOR_STRING_IDX_INTERFACE_CONTROL,
	USB_DESCRIPTOR_STRING_IDX_INTERFACE_DATA,
} usb_descriptor_string_idx_t;

const usb_descriptor_device_t device_descriptor = {
    .bLength		= sizeof(usb_descriptor_device_t),
    .bDescriptorType	= USB_DESCRIPTOR_DEVICE,
    .bcdUSB		= USB_REVISION,
    .bDeviceClass	= USB_CP210X_DEVICE_CLASS,
    .bDeviceSubClass	= USB_CP210X_DEVICE_SUBCLASS,
    .bDeviceProtocol	= USB_CP210X_DEVICE_PROTOCOL,
    .bMaxPacketSize0	= USB_EP0_SIZE,
    .idVendor		= USB_CP210X_ID_VENDOR,
    .idProduct		= USB_CP210X_ID_PRODUCT,
    .bcdDevice		= 0x0001,
    .iManufacturer	= USB_STRING_IDX_TO_NUM(USB_DESCRIPTOR_STRING_IDX_MFR),
    .iProduct		= USB_STRING_IDX_TO_NUM(USB_DESCRIPTOR_STRING_IDX_PRODUCT),
    .iSerialNumber	= USB_STRING_IDX_TO_NUM(USB_DESCRIPTOR_STRING_IDX_SERIALNUMBER),
    .bNumConfigurations = 1};

const usb_descriptor_endpoint_t usb_descriptor_endpoint1[] = {
    {.bLength		  = sizeof(usb_descriptor_endpoint_t),
     .bDescriptorType	  = USB_DESCRIPTOR_ENDPOINT,
     .EndpointNumber	  = 0x01,
     .Direction		  = USB_ENDPOINT_DIRECTION_IN,
     .TransferType	  = USB_TRANSFER_TYPE_BULK,
     .SynchronizationType = USB_SYNCHRONIZATION_TYPE_NO_SYNCHRONIZATION,
     .UsageType		  = USB_USAGE_TYPE_DATA_ENDPOINT,
     .wMaxPacketSize	  = 64,
     .bInterval		  = 0},
    {.bLength		  = sizeof(usb_descriptor_endpoint_t),
     .bDescriptorType	  = USB_DESCRIPTOR_ENDPOINT,
     .EndpointNumber	  = 0x01,
     .Direction		  = USB_ENDPOINT_DIRECTION_OUT,
     .TransferType	  = USB_TRANSFER_TYPE_BULK,
     .SynchronizationType = USB_SYNCHRONIZATION_TYPE_NO_SYNCHRONIZATION,
     .UsageType		  = USB_USAGE_TYPE_DATA_ENDPOINT,
     .wMaxPacketSize	  = 64,
     .bInterval		  = 0}};

const usb_descriptor_endpoint_t usb_descriptor_endpoint2[] = {
    {.bLength		  = sizeof(usb_descriptor_endpoint_t),
     .bDescriptorType	  = USB_DESCRIPTOR_ENDPOINT,
     .EndpointNumber	  = 0x02,
     .Direction		  = USB_ENDPOINT_DIRECTION_IN,
     .TransferType	  = USB_TRANSFER_TYPE_BULK,
     .SynchronizationType = USB_SYNCHRONIZATION_TYPE_NO_SYNCHRONIZATION,
     .UsageType		  = USB_USAGE_TYPE_DATA_ENDPOINT,
     .wMaxPacketSize	  = 64,
     .bInterval		  = 0},
    {.bLength		  = sizeof(usb_descriptor_endpoint_t),
     .bDescriptorType	  = USB_DESCRIPTOR_ENDPOINT,
     .EndpointNumber	  = 0x02,
     .Direction		  = USB_ENDPOINT_DIRECTION_OUT,
     .TransferType	  = USB_TRANSFER_TYPE_BULK,
     .SynchronizationType = USB_SYNCHRONIZATION_TYPE_NO_SYNCHRONIZATION,
     .UsageType		  = USB_USAGE_TYPE_DATA_ENDPOINT,
     .wMaxPacketSize	  = 64,
     .bInterval		  = 0}};

const usb_descriptor_interface_t usb_descriptor_interface[] = {
    {.bLength		 = sizeof(usb_descriptor_interface_t),
     .bDescriptorType	 = USB_DESCRIPTOR_INTERFACE,
     .bInterfaceNumber	 = 0,
     .bAlternateSetting	 = 0,
     .bNumEndpoints	 = ARRAY_SIZE(usb_descriptor_endpoint1),
     .bInterfaceClass	 = USB_CP210X_INTERFACE_CLASS,
     .bInterfaceSubClass = USB_CP210X_INTERFACE_SUBCLASS,
     .bInterfaceProtocol = USB_CP210X_INTERFACE_PROTOCOL,
     .iInterface	 = USB_STRING_IDX_TO_NUM(USB_DESCRIPTOR_STRING_IDX_INTERFACE_DATA)},
    {.bLength		 = sizeof(usb_descriptor_interface_t),
     .bDescriptorType	 = USB_DESCRIPTOR_INTERFACE,
     .bInterfaceNumber	 = 1,
     .bAlternateSetting	 = 0,
     .bNumEndpoints	 = ARRAY_SIZE(usb_descriptor_endpoint2),
     .bInterfaceClass	 = USB_CP210X_INTERFACE_CLASS,
     .bInterfaceSubClass = USB_CP210X_INTERFACE_SUBCLASS,
     .bInterfaceProtocol = USB_CP210X_INTERFACE_PROTOCOL,
     .iInterface	 = USB_STRING_IDX_TO_NUM(USB_DESCRIPTOR_STRING_IDX_INTERFACE_DATA)}};

const usb_descriptor_config_t usb_descriptor_config = {
    .bLength	     = sizeof(usb_descriptor_config_t),
    .bDescriptorType = USB_DESCRIPTOR_CONFIGURATION,
    .wTotalLength    = sizeof(usb_descriptor_config) + sizeof(usb_descriptor_interface) +
		    sizeof(usb_descriptor_endpoint1) + sizeof(usb_descriptor_endpoint2),
    .bNumInterfaces	 = ARRAY_SIZE(usb_descriptor_interface),
    .bConfigurationValue = 1,
    .iConfiguration	 = 0,
    .ReservedAttr	 = true,
    .bMaxPower		 = USB_POWER_MA(100)};

usb_sendbuffer_chunks_t usb_full_configuraion_descriptor_buffer = {
    .chunk_count = 5,
    .chunks	 = {
	     {.address = (void *) &usb_descriptor_config, .size = sizeof(usb_descriptor_config)},
	     {.address = (void *) &usb_descriptor_interface[0], .size = sizeof(usb_descriptor_interface[0])},
	     {.address = (void *) &usb_descriptor_endpoint1, .size = sizeof(usb_descriptor_endpoint1)},
	     {.address = (void *) &usb_descriptor_interface[1], .size = sizeof(usb_descriptor_interface[1])},
	     {.address = (void *) &usb_descriptor_endpoint2, .size = sizeof(usb_descriptor_endpoint2)},
	 }};

usb_string_list_item_t usb_string_list_item_en = {.wLangID	= USB_DESCRIPTOR_LANGUAGE_ID_ENGLISH_JAMAICA,
						  .string_count = 5,
						  .strings	= {
							   [USB_DESCRIPTOR_STRING_IDX_MFR]		 = L"RED",
							   [USB_DESCRIPTOR_STRING_IDX_PRODUCT]		 = L"ECHO MACHINE",
							   [USB_DESCRIPTOR_STRING_IDX_SERIALNUMBER]	 = L"12345",
							   [USB_DESCRIPTOR_STRING_IDX_INTERFACE_DATA]	 = L"DATA",
							   [USB_DESCRIPTOR_STRING_IDX_INTERFACE_CONTROL] = L"CONTROL",
						       }};

usb_string_list_item_t usb_string_list_item_ru = {.wLangID	= USB_DESCRIPTOR_LANGUAGE_ID_RUSSIAN,
						  .string_count = 5,
						  .strings	= {
							   [USB_DESCRIPTOR_STRING_IDX_MFR]	    = L"КРАСНЫЙ",
							   [USB_DESCRIPTOR_STRING_IDX_PRODUCT]	    = L"МАШИНА ЭХО",
							   [USB_DESCRIPTOR_STRING_IDX_SERIALNUMBER] = L"12345",
							   [USB_DESCRIPTOR_STRING_IDX_INTERFACE_DATA] = L"ДАННЫЕ",
							   [USB_DESCRIPTOR_STRING_IDX_INTERFACE_CONTROL] = L"КОНТРОЛЬ",
						       }};

const usb_string_list_t usb_string_list = {.language_count = 2,
					   .langs	   = {
						&usb_string_list_item_en,
						&usb_string_list_item_ru,
					    }};
