# toolchain
CROSS_COMPILE := arm-none-eabi-
CC := $(CROSS_COMPILE)gcc
AS := $(CROSS_COMPILE)as
OBGDUMP := $(CROSS_COMPILE)objdump
OBGCOPY := $(CROSS_COMPILE)objcopy
NM := $(CROSS_COMPILE)nm
GDB := gdb-multiarch
OPENOCD ?= openocd
NEWLIB_LIB ?= ../newlib/newlib/install/arm-none-eabi/lib/
NEWLIB_INCLUDE ?= ../newlib/newlib/install/arm-none-eabi/include/

include config.mk

AARCH = -mcpu=cortex-m3
CCWARNINGS = -Wall -Wextra
ASWARNINGS = --fatal-warnings
ARMMODE = -mthumb
ABI = -mabi=aapcs-linux

# flags
GCCOPTS = -nostartfiles -nostdlib -ffreestanding -nodefaultlibs -fno-zero-initialized-in-bss -fstack-usage -u__isr_vector
LDFLAGS = -T../hal/link/stm32f10x.ld -static -L$(NEWLIB_LIB) -L../hal -l$(CHIP) -lgcc -lc $(GCCOPTS) \
	-Wl,-no-enum-size-warning -Wl,--defsym,__ramsize=$(RAMSIZE) -Wl,--defsym,__flashsize=$(FLASHSIZE) \
	-Wl,--defsym,__shared_ramsize=$(SHAREDRAMSIZE)
ASFLAGS = $(ARMMODE) $(AARCH)
CCFLAGS = -std=gnu2x $(GCCOPTS) $(ARMMODE) $(AARCH) $(ABI) $(DEF_EXTRA)
INCLUDE = -I$(NEWLIB_INCLUDE) -I../hal/include
INCLUDE_FILES := $(wildcard *.h ../input_parser/*.h)

ifdef FPU
CCFLAGS += -D__FPU_PRESENT
else
ABI += -mfloat-abi=soft
endif

ifndef NDEBUG
CCFLAGS += -O0 -ggdb
ASFLAGS += -gstabs+ -g
else
CCFLAGS += -Os
endif

CCFLAGS += -D$(CHIP)

# objects
TARGET := main
SRC := $(wildcard *.c *.s)
SRC += $(wildcard ../input_parser/input_parser.c)
OBJS := $(patsubst %.s,%.o,$(SRC:%.c=%.o))
BIN := $(TARGET).elf
HAL_LIB := hal/lib$(CHIP).a
LIBC_LIB := $(NEWLIB_LIB)/libc.a
OCD_CONF := ../openocd.cfg


$(BIN): $(LIBC_LIB) $(HAL_LIB) $(OBJS) $(LINKSCR) $(INCLUDE_FILES)
	$(CC) $(CCFLAGS) -o $@ $(OBJS) $(LDFLAGS)

$(HAL_LIB): $(LIBC_LIB)
	make -C ../hal CHIP=$(CHIP)

$(LIBC_LIB):
	mkdir -p ../newlib/newlib/build
	cd ../newlib/newlib/build; \
	../configure \
		--prefix=$(PWD)/../newlib/newlib/install \
		--build=$(shell uname -m) \
		--host=arm-none-eabi \
		CFLAGS="$(AARCH) $(ARMMODE) $(ABI)" \
		CC=$(CC) \
		LD=$(LD)
	make -C ../newlib/newlib/build -j$(nproc)
	make -C ../newlib/newlib/build install

$(TARGET).bin: $(BIN)
	$(OBGCOPY) $< -O binary $@

%.o: %.s
	$(AS) -c $(ASFLAGS) $(ASWARNINGS) $< -o $@

%.o: %.c
	$(CC) -c $(INCLUDE) $(CCFLAGS) $(CCWARNINGS) $< -o $@

.PHONY: headers
headers: $(BIN)
	$(OBGDUMP) -h $<

.PHONY: disassemble
disassemble: $(BIN)
	$(OBGDUMP) -wDz $<

.PHONY: symbols
symbols: $(BIN)
	$(NM) $<

.PHONY: debug
debug: $(BIN)
	$(GDB) -ix ../gdbinit -tui -ex 'tui reg all' \
	-ex 'file $^' \
	-ex 'target extended-remote | $(OPENOCD) -f $(OCD_CONF)' \
	-ex 'mflash_load $^'

.PHONY: clean
clean:
	make -C ../hal clean
	rm -f $(OBJS)
	rm -f $(BIN)
	rm -f $(TARGET).bin
	rm -f *.log
	rm -f *~
