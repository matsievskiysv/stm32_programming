#pragma once

#include "../input_parser/input_parser.h"

#include <stddef.h>
#include <stdint.h>

#ifndef LED_PORT
#define LED_PORT GPIO_C
#endif // LED_PORT

#ifndef LED_PIN
#define LED_PIN 13
#endif // LED_PIN

#define FREQ		 8000000
#define BAUD		 115200
#define UART_PORT	 USART1
#define UART_GPIO_PORT	 GPIO_A
#define UART_PIN_TX	 9
#define UART_PIN_RX	 10
#define DATA_BUFFER_SIZE 16

char data_buffer[DATA_BUFFER_SIZE] = {0};

char prompt_string[] = "\r\n>";

// void change_state(enum state new);

// void transmit_buffer(uint8_t *buffer, size_t count);

// void send_string(char* string);

// void echo_received(void);

// void start_listen(void);

// void process_input(void);

// void run_callback(void);

// void help_function(void);

// void hello_function(void);

// void bye_function(void);

// void flash_size_function(void);

// void chip_id_function(void);

// iprs_state parser_state = {0};
// iprs_string commands[] = {
// 	{.string = "help"},
// 	{.string = "hello"},
// 	{.string = "bye"},
// 	{.string = "flash size"},
// 	{.string = "unique id"},
// };

// void (*const functions[])(void) = {
// 	help_function,
// 	hello_function,
// 	bye_function,
// 	flash_size_function,
// 	chip_id_function
// };
