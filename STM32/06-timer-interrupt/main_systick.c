#include "memmap/gpio.h"
#include "memmap/rcc.h"
#include "memmap/systick.h"

volatile unsigned long *const gpioc_odr = (unsigned long *) (GPIO_C + GPIOx_ODR);

void
systick_handler(void)
{
	// toggle LED
	*gpioc_odr ^= (1 << 13);
}

int __attribute__((noreturn)) main(void)
{
	volatile unsigned long *const rcc_cr = (unsigned long *) (RCC + RCC_CR);
	// enable 8MHz oscillator
	*rcc_cr |= (1 << 0);
	// wait for 8MHz oscillator to stabilize
	while (!(*rcc_cr & (1 << 1))) {}
	// enable GPIOC clock
	volatile unsigned long *const rcc_apb2enr = (unsigned long *) (RCC + RCC_APB2ENR);
	*rcc_apb2enr |= (1 << 4);

	// set systick reload value
	volatile unsigned long *const syst_rvr = (unsigned long *) (SYST_RVR);
	*syst_rvr			       = 0x7a1200UL;
	// enable interrupt and start systick
	volatile unsigned long *const syst_csr = (unsigned long *) (SYST_CSR);
	*syst_csr |= 0b111;
	// set GPIOC CRH
	volatile unsigned long *const gpioc_crh = (unsigned long *) (GPIO_C + GPIOx_CRH);
	*gpioc_crh				= (*gpioc_crh & ~(0b1111 << 20)) | (0b0010 << 20);
	// set GPIOC output value
	volatile unsigned long *const gpioc_odr = (unsigned long *) (GPIO_C + GPIOx_ODR);
	// disable LED
	*gpioc_odr |= (1 << 13);

	for (;;) {}
}
