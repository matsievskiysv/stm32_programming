#include "io.h"

void
sleep_cycle(unsigned int cycles)
{
	for (unsigned int i = 0; i < cycles; i++) {}
}

unsigned long
factorial(unsigned int n)
{
	if (n > 1)
		return n * factorial(n - 1);
	else
		return 1;
}

int __attribute__((noreturn)) main(void)
{
	for (;;) {
		for (int i = 0; i < 100; i++) {
			factorial(i);
		}
	}
}
