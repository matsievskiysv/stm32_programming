#pragma once

void *memcpy(void *dst, const void *src, unsigned int n);
void *memset(void *s, unsigned char c, unsigned int n);
