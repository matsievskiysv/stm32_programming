#pragma once

#define NVIC_ISER 0xE000##E100##UL
#define NVIC_ICER 0xE000##E180##UL
#define NVIC_ISPR 0xE000##E200##UL
#define NVIC_ICPR 0xE000##E280##UL
#define NVIC_IABR 0xE000##E300##UL
#define NVIC_IP	  0xE000##E400##UL
#define NVIC_STIR 0xE000##EF00##UL

#define NVIC_REG_OFFSET(N) (N >> 5)
#define NVIC_BIT(N)	   (N & 0x1F##UL)
