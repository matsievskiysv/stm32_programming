#include "memmap/adc.h"
#include "memmap/dma.h"
#include "memmap/gpio.h"
#include "memmap/nvic.h"
#include "memmap/rcc.h"
#include "memmap/systick.h"

#define NULL 0

void config_oscillator(void);
void config_systick(void);
void config_temp_sensor(void);
void config_dma_adc(void);
void calibrate_temp_sensor(void);
void start_temp_sensor(void);

struct job_list_t {
	unsigned short int count;
	unsigned short int current;
	void (*callback)(void);
};

unsigned short int temp_sens_raw[2] = {0};
long int	   temp		    = 0;
struct job_list_t  job_list[]	    = {
	   {.count = 60, .current = 0, .callback = calibrate_temp_sensor},
	   {.count = 10, .current = 0, .callback = start_temp_sensor},
	   {0},
};

void
config_oscillator(void)
{
	volatile unsigned long *const rcc_cr = (unsigned long *) (RCC + RCC_CR);
	// enable 8MHz oscillator
	*rcc_cr |= (1 << 0);
	// wait for 8MHz oscillator to stabilize
	while (!(*rcc_cr & (1 << 1))) {}
}

void
config_led(void)
{
	// enable GPIOC clock
	volatile unsigned long *const rcc_apb2enr = (unsigned long *) (RCC + RCC_APB2ENR);
	*rcc_apb2enr |= (1 << 4);
	// set GPIOC CRH
	volatile unsigned long *const gpioc_crh = (unsigned long *) (GPIO_C + GPIOx_CRH);
	*gpioc_crh				= (*gpioc_crh & ~(0b1111 << 20)) | (0b0010 << 20);
	// set GPIOC output value
	volatile unsigned long *const gpioc_odr = (unsigned long *) (GPIO_C + GPIOx_ODR);
	// disable LED
	*gpioc_odr |= (1 << 13);
}

void
toggle_led(void)
{
	// set GPIOC output value
	volatile unsigned long *const gpioc_odr = (unsigned long *) (GPIO_C + GPIOx_ODR);
	// toggle LED
	*gpioc_odr ^= (1 << 13);
}

void
config_systick(void)
{
	// set systick reload value
	volatile unsigned long *const syst_rvr = (unsigned long *) (SYST_RVR);
	*syst_rvr			       = 0x7a1200UL;
	// enable interrupt and start systick
	volatile unsigned long *const syst_csr = (unsigned long *) (SYST_CSR);
	*syst_csr |= 0b111;
}

void
config_temp_sensor(void)
{
	// enable ADC1 clock
	volatile unsigned long *const rcc_apb2enr = (unsigned long *) (RCC + RCC_APB2ENR);
	*rcc_apb2enr |= (1 << 9);
	// set ADC1 clock prescaler 1/8
	volatile unsigned long *const rcc_cfgr = (unsigned long *) (RCC + RCC_CFGR);
	*rcc_cfgr |= (0b11 << 14);
	// setup ADC1
	volatile unsigned long *const adc_cr1 = (unsigned long *) (ADC1 + ADCx_CR1);
	*adc_cr1 |= (1 << 8); // SCAN
	volatile unsigned long *const adc_cr2 = (unsigned long *) (ADC1 + ADCx_CR2);
	*adc_cr2 |= (1 << 23)	// TSVREFE
		    | (1 << 8)	// DMA
		    | (1 << 0); // ADON
	// setup ADC1 maximum sample time
	volatile unsigned long *const adc_smpr1 = (unsigned long *) (ADC1 + ADCx_SMPR1);
	*adc_smpr1 |= (0b111 << 21) | (0b111 << 18);
	// set ADC1 sequence length
	volatile unsigned long *const adc_sqr1 = (unsigned long *) (ADC1 + ADCx_SQR1);
	*adc_sqr1 |= (1 << 20);
	// setup ADC1 sequence
	volatile unsigned long *const adc_sqr3 = (unsigned long *) (ADC1 + ADCx_SQR3);
	*adc_sqr3 |= (16 << 0) | (17 << 5);
}

void
config_dma_adc(void)
{
	// enable DMA1 clock
	volatile unsigned long *const rcc_ahbenr = (unsigned long *) (RCC + RCC_AHBENR);
	*rcc_ahbenr |= (1 << 0);
	volatile unsigned long *const dma1_ccr1 = (unsigned long *) (DMA1 + DMA_CH(1) + DMAx_CCRx);
	// disable channel 1
	*dma1_ccr1 &= ~(1 << 0); // EN
	// configure channel 1
	*dma1_ccr1 |= (0b01 << 12)   // PL
		      | (0b01 << 10) // MSIZE
		      | (0b01 << 8)  // PSIZE
		      | (1 << 7)     // MINC
		      | (1 << 5)     // CIRC
		      | (1 << 3)     // TEIE
		      | (1 << 1);    // TCIE
	// set peripheral address to ADC1 data register
	volatile unsigned long *const dma1_cpar1 = (unsigned long *) (DMA1 + DMA_CH(1) + DMAx_CPARx);
	*dma1_cpar1				 = ADC1 + ADCx_DR;
	// set memory address to our variable
	volatile unsigned long *const dma1_cmar1 = (unsigned long *) (DMA1 + DMA_CH(1) + DMAx_CMARx);
	*dma1_cmar1				 = (unsigned long) &temp_sens_raw;
	// set transfer size
	volatile unsigned long *const dma1_cndtr1 = (unsigned long *) (DMA1 + DMA_CH(1) + DMAx_CNDTRx);
	*dma1_cndtr1				  = 2;
	// enable DMA interrupt (#11)
	volatile unsigned long *const nvic_iser_tim2 = (unsigned long *) (NVIC_ISER + NVIC_REG_OFFSET(11));
	*nvic_iser_tim2 |= NVIC_BIT(11);
	// enable channel 1
	*dma1_ccr1 |= (1 << 0); // EN
}

void
calibrate_temp_sensor(void)
{
	volatile unsigned long *const adc_cr2 = (unsigned long *) (ADC1 + ADCx_CR2);
	// Start calibration
	*adc_cr2 |= (1 << 2);
	while (*adc_cr2 & (1 << 2)) {} // wait until complete
}

void
start_temp_sensor(void)
{
	volatile unsigned long *const adc_sr  = (unsigned long *) (ADC1 + ADCx_SR);
	volatile unsigned long *const adc_cr2 = (unsigned long *) (ADC1 + ADCx_CR2);
	*adc_sr &= ~0x1f;     // reset status register
	*adc_cr2 |= (1 << 0); // Start conversion
}

void
systick_handler(void)
{
	for (unsigned int i = 0; job_list[i].callback != NULL; i++) {
		if (job_list[i].current == 0) {
			job_list[i].callback();
			job_list[i].current = job_list[i].count;
		} else {
			job_list[i].current--;
		}
	}
}

void
dma1_channel1_irqhandler(void)
{
	volatile unsigned long *const dma1_isr = (unsigned long *) (DMA1 + DMA_ISR);
	if (*dma1_isr & (1 << 3)) {
		// handle transfer error
		temp = -273;
	} else {
#ifdef __FPU_PRESENT
		temp = (1.43 - (1.2 * temp_sens_raw[0]) / temp_sens_raw[1]) / 0.0043 * 1000 + 25000;
#else
		temp = (14300 - (12000L * temp_sens_raw[0]) / temp_sens_raw[1]) * 1000 / 43 + 25000;
#endif
	}
	volatile unsigned long *const dma1_ifcr = (unsigned long *) (DMA1 + DMA_IFCR);
	*dma1_ifcr				= 0x1f; // reset interrupts
	// Toggle LED
	toggle_led();
}

int __attribute__((noreturn)) main(void)
{
	temp_sens_raw[0] = 0;
	temp_sens_raw[1] = 0;

	config_oscillator();
	config_led();
	config_dma_adc();
	config_temp_sensor();
	config_systick();

	for (;;) {}
}
