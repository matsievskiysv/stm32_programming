#pragma once

#define EXTI 0x4001##0400##UL

#define EXTI_IMR   0x00##UL
#define EXTI_EMR   0x04##UL
#define EXTI_RTSR  0x08##UL
#define EXTI_FTSR  0x0C##UL
#define EXTI_SWIER 0x10##UL
#define EXTI_PR	   0x14##UL
