#include "util.h"

void *
memcpy(void *dst, const void *src, unsigned int n)
{
	unsigned int	     i, m;
	unsigned long	    *wdst = dst;
	const unsigned long *wsrc = src;
	unsigned char	    *cdst, *csrc;

	for (i = 0, m = n / sizeof(long); i < m; i++)
		*(wdst++) = *(wsrc++);

	cdst = (unsigned char *) wdst;
	csrc = (unsigned char *) wsrc;

	for (i = 0, m = n % sizeof(long); i < m; i++)
		*(cdst++) = *(csrc++);

	return dst;
}

void *
memset(void *s, unsigned char c, unsigned int n)
{
	unsigned int	   i, m;
	unsigned long int *wdst = s;
	unsigned char	  *cdst;
	unsigned char	   csrc	   = c;
	unsigned char	   _wsrc[] = {csrc, csrc, csrc, csrc};
	unsigned long int  wsrc	   = *(unsigned long int *) _wsrc;

	for (i = 0, m = n / sizeof(long); i < m; i++)
		*(wdst++) = wsrc;

	cdst = (unsigned char *) wdst;

	for (i = 0, m = n % sizeof(long); i < m; i++)
		*(cdst++) = c;

	return s;
}
