# Shortcuts

| Shortcut | Description |
|:-:|:-:|
| <kbd>CTRL+A</kbd> | move to beginning of line |
| <kbd>CTRL+B</kbd> | moves backward one character |
| <kbd>CTRL+C</kbd> | halts the current command |
| <kbd>CTRL+D</kbd> | deletes one character backward or logs out of current session, similar to exit |
| <kbd>CTRL+E</kbd> | moves to end of line |
| <kbd>CTRL+F</kbd> | moves forward one character |
| <kbd>CTRL+G</kbd> | aborts the current editing command and ring the terminal bell |
| <kbd>CTRL+J</kbd> | same as RETURN |
| <kbd>CTRL+L</kbd> | clears screen and redisplay the line |
| <kbd>CTRL+M</kbd> | same as RETURN |
| <kbd>CTRL+N</kbd> | next line in command history |
| <kbd>CTRL+O</kbd> | same as RETURN, then displays next line in history file |
| <kbd>CTRL+P</kbd> | previous line in command history |
| <kbd>CTRL+R</kbd> | searches backward |
| <kbd>CTRL+S</kbd> | searches forward |
| <kbd>CTRL+T</kbd> | transposes two characters |
| <kbd>CTRL+U</kbd> | kills backward from point to the beginning of line |
| <kbd>CTRL+V</kbd> | makes the next character typed verbatim |
| <kbd>CTRL+W</kbd> | kills the word behind the cursor |
| <kbd>CTRL+X</kbd> | lists the possible filename completefions of the current word |
| <kbd>CTRL+Y</kbd> | retrieves (yank) last item killed |
| <kbd>CTRL+Z</kbd> | stops the current command, resume with fg in the foreground or bg in the background |
| <kbd>DELETE</kbd> | deletes one character backward |

| Variables | Description |
|:-:|:-:|
| `!!` | repeats the last command |
| `$HOME` | location of the home directory |
| `~` | alias to `HOME` |
| `$USER` | name of user |
| `$SHELL` | shell you're using |
| `$BASH_VERSION` | displays bash version |

# Bash Basics

**display all environment variables**
```bash
export
```

**if you want to use bash (type exit to go back to your normal shell)**
```bash
bash
```

**find out where bash is on your system**
```bash
whereis bash
```


**clear content on window (hide displayed lines)**
```bash
clear
```


## File Commands


**list your files**
```bash
ls
```

**list your files in 'long format', which contains the exact size of the file, who owns the file and who has the right to look at it, and when it was last modified**
```bash
ls -l
```

**list all files, including hidden files**
```bash
ls -a
```

**create symbolic link to file**
```bash
ln -s <filename> <link>
```

**create or updates your file**
```bash
touch <filename>
```

**place standard input into file**
```bash
cat > <filename>
```

**show the first part of a file (move with space and type q to quit)**
```bash
more <filename>
```

**output the first 10 lines of file**
```bash
head <filename>
```

**output the last 10 lines of file (useful with -f option)**
```bash
tail <filename>
```

**let you create and edit a file**
```bash
emacs <filename>
```

**move a file**
```bash
mv <filename1> <filename2>
```

**copie a file**
```bash
cp <filename1> <filename2>
```

**remove a file**
```bash
rm <filename>
```

**compare files, and shows where they differ**
```bash
diff <filename1> <filename2>
```

**tell you how many lines, words and characters there are in a file**
```bash
wc <filename>
```

**let you change the read, write, and execute permissions on your files**
```bash
chmod -options <filename>
```

**compresse files**
```bash
gzip <filename>
```

**uncompresse files compressed by gzip**
```bash
gunzip <filename>
```

**let you look at gzipped file without actually having to gunzip it**
```bash
gzcat <filename>
```

**print the file**
```bash
lpr <filename>
```

**check out the printer queue**
```bash
lpq
```

**remove something from the printer queue**
```bash
lprm <jobnumber>
```

**convert plain text files into postscript for printing and gives you some options for formatting**
```bash
genscript
```

**print .dvi files (i.e. files produced by LaTeX)**
```bash
dvips <filename>
```

**look for the string in the files**
```bash
grep <pattern> <filenames>
```

**search recursively for pattern in directory**
```bash
grep -r <pattern> <dir>
```

## Directory Commands


**make a new directory**
```bash
mkdir <dirname>
```

**change to home**
```bash
cd
```

**change directory**
```bash
cd <dirname>
```

**tell you where you currently are**
```bash
pwd
```

## System Info

**return your username**
```bash
whoami
```

**let you change your password**
```bash
passwd
```

**show the current date and time**
```bash
date
```

**show the month's calendar**
```bash
cal
```

**show current uptime**
```bash
uptime
```

**display whois online**
```bash
w
```

**display information about user**
```bash
finger <user>
```

**show kernel information**
```bash
uname -a
```

**show the manual for specified command**
```bash
man <command>
```

**show disk usage**
```bash
df
```

**show the disk usage of the files and directories in filename (du -s give only a total)**
```bash
du <filename>
```

**list your last logins**
```bash
last <yourUsername>
```

**list your processes**
```bash
ps -u yourusername
```

**kill (ends) the processes with the ID you gave**
```bash
kill <PID>
```

**kill all processes with the name**
```bash
killall <processname>
```

**display your currently active processes**
```bash
top
```

**list stopped or background jobs ; resume a stopped job in the background**
```bash
bg
```

**bring the most recent job in the foreground**
```bash
fg
```

**bring job to the foreground**
```bash
fg <job>
```


**ping host and outputs results**
```bash
ping <host>
```

**get whois information for domain**
```bash
whois <domain>
```

**get DNS information for domain**
```bash
dig <domain>
```

**reverse lookup host**
```bash
dig -x <host>
```

**download file**
```bash
wget <file>
```
