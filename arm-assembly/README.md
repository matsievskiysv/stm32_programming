% ARM assembly

This folder contains ARM assembly projects.
These projects use [QEMU](https://www.qemu.org/) virtual machine.
Most of the projects are written for Cortex-M3 processor in Thumb mode.
Floating point arithmetic projects are written for Cortex-A7 processor in ARM mode.


1. [toolchain](./01-toolchain) - basic development tools: compiler, debugger etc.
1. [basic-operations](./02-basic-operations) - basic processor data operations
1. [floating-point](./03-floating-point) - usage of floating point unit (FPU)
1. [functions](./04-functions) - AAPCS ABI application

[documents](./documents) folder contains relevant documentation
