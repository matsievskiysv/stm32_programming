int
foo(int a)
{
	int b = a * 2;
	return b;
}

float
bar(int a)
{
	float b = 1 / (float) a;
	return b;
}
