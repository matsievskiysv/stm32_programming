	.syntax unified		@ use unified assembly syntax

	.section .isr_vector,"a" @ reset vector section
	b _reset		@ entry point
	b _reset 		@ Reset: relative branch allows remap
	b .			@ Undefined Instruction
	b .			@ Software Interrupt
	b .			@ Prefetch Abort
	b .			@ Data Abort
	b .			@ Reserved
	b .			@ IRQ
	b .			@ FIQ

	.section .text		@ text section start
_reset:

	.extern _stack_start
_init_stack:
	ldr sp, =_stack_start
_enable_fpu:			@ enable fpu
	mrc p15, 0, r0, c1, c0, 2
	orr r0, r0, #0x300000 @ single precision
	@@ orr r0, r0, #0xC00000 @ double precision
	mcr p15, 0, r0, c1, c0, 2
	mov r0, #0x40000000
	fmxr fpexc, r0

	.global _start		@ make _start visible
_start:
	@@ Calculate e number
	@@ $e^x = 1 + \sum_{n=1}^{\infty}{\frac{x^n}{n!}}$
	@@ In this case $x=1$
	mov r8, #1
	vmov s0, r8		@ accumulator
	mov r0, #10		@ number of iterations
	mov r1, #1		@ counter
0:
	bl factorial		@ s1 = r1!
	mov r8, #1
	vmov s5, r8
	vdiv.f32 s5, s5, s1	@ s5 = 1/s1
	vadd.f32 s0, s0, s5	@ s0 = s0 + s5

	add r1, r1, #1		@ while loop
	cmp r1, r0
	it le
	ble 0b
	nop

	b wait
	nop

factorial:
	@@ compute factorial in a loop
	@@ s1 - accumulator
	@@ s5 - temp value
	@@ r1 - argument
	@@ ip - counter
	mov ip, #1
	mov r8, #1
	vmov.f32 s1, r8
0:
	cmp ip, r1
	it eq
	bxeq lr
	nop

	add ip, ip, #1
	vmov s5, ip
	vcvt.f32.u32 s5, s5
	vmul.f32 s1, s1, s5
	b 0b
	nop

wait:				@ endless loop
	add ip, ip, #1
	b wait
	nop

	.end
